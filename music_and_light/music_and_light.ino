/*
  Melody

 Plays a melody

 circuit:
 * 8-ohm speaker on digital pin 8

 created 21 Jan 2010
 modified 30 Aug 2011
 by Tom Igoe

This example code is in the public domain.

 http://www.arduino.cc/en/Tutorial/Tone

 */
#include "pitches.h"
const int SPEAKER_PIN = 8;
const int RED_PIN = 9;
const int GREEN_PIN = 10;
const int BLUE_PIN = 11;
const int BUTTON1_PIN = 2;
const int BUTTON2_PIN = 3;
//ideas
//light show with rgb led
//selectable song, possibly using logic
//vibrato
//chords

// notes in the melody:
int melody[] = {
  NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4, 8, 8, 4, 4, 4, 4, 4
};

void setup() {
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);
  pinMode(BUTTON1_PIN, INPUT);
  pinMode(BUTTON2_PIN, INPUT);
  pinMode(SPEAKER_PIN, OUTPUT);
  
  /*
  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < 8; thisNote++) {

    // to calculate the note duration, take one second
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(8, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(8);
    
  }*/
}

typedef struct musicalNote{
  int note;
  int duration;
};

void hollyJollySectionA(){
  int melody[] = { NOTE_E4, NOTE_G4, NOTE_C5, NOTE_C5, NOTE_B4, NOTE_B4, NOTE_A4, NOTE_E4, NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, NOTE_G4, NOTE_G4, NOTE_B3, -1};
  int noteDurations[] = {8, 8, 4, 4, 4, 4, 4, 2, 8, 8, 4, 4, 4, 4, 1};
  int colorTones[] = {BLUE_PIN, GREEN_PIN, RED_PIN, RED_PIN, GREEN_PIN, GREEN_PIN, BLUE_PIN, RED_PIN, BLUE_PIN, GREEN_PIN,GREEN_PIN, GREEN_PIN, RED_PIN, RED_PIN, GREEN_PIN};

  playSection(melody, noteDurations, colorTones);
  /*
  for(int curNote = 0; melody[curNote] != -1; curNote++){
    //unsigned int noteDuration = ((1000 * 60) / tempo) / (noteDurations[curNote] / 4);
    int noteDuration = 1000 / noteDurations[curNote];
    tone(SPEAKER_PIN, melody[curNote], noteDuration);
    digitalWrite(colorTones[curNote], HIGH);
    delay(noteDuration * 1.30);
    digitalWrite(colorTones[curNote], LOW);
    noTone(SPEAKER_PIN);
  }*/
}

void hollyJollySectionB(){
  int melody[] = {NOTE_B4,NOTE_B4,NOTE_B4,NOTE_A4, NOTE_G4, NOTE_G4, NOTE_G4, NOTE_E4, -1};
  int noteDurations[] = {4, 4, 3, 8, 4, 4, 3, 8, };
  int colorTones[] = {GREEN_PIN, GREEN_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, BLUE_PIN, BLUE_PIN, RED_PIN};
  
  playSection(melody, noteDurations, colorTones);
}

void hollyJollySectionB1(){

  int melody[] = {NOTE_G4, NOTE_G4, NOTE_F4, NOTE_G4, NOTE_E4, -1};
  int noteDurations[] = {4, 4, 4, 4, 1/*technically dotted half note*/};
  int colorTones[] = {GREEN_PIN, GREEN_PIN, RED_PIN, GREEN_PIN, RED_PIN};

  playSection(melody, noteDurations, colorTones);
}

void hollyJollySectionB2(){
  int melody[] = {NOTE_G4, NOTE_G4, NOTE_F4, NOTE_D4, NOTE_C4, -1};
  int noteDurations[] = {4, 4, 4, 4, 1};
  int colorTones[] = {GREEN_PIN, GREEN_PIN, RED_PIN, GREEN_PIN, RED_PIN};

  playSection(melody, noteDurations, colorTones);
}

void hollyJollySectionC(){
  int melody[] = {NOTE_C5, NOTE_A4, NOTE_C5, NOTE_B4, NOTE_G4, NOTE_G4, NOTE_A4, NOTE_F4, NOTE_F4, NOTE_A4, NOTE_G4, NOTE_F4, NOTE_D4, NOTE_F4, NOTE_E4, NOTE_A4, NOTE_A4, NOTE_D4, NOTE_D4, NOTE_E4, NOTE_FS4, NOTE_G4, -1};
  int noteDurations[] = {2, 3, 8, 4, 4, 2, 4, 4, 4, 4, 1, 2, 3, 8, 4, 4, 2, 4, 4, 4, 4, 1};
  int colorTones[] = {BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, RED_PIN, RED_PIN, BLUE_PIN, RED_PIN, RED_PIN, BLUE_PIN, RED_PIN, RED_PIN, GREEN_PIN, BLUE_PIN, RED_PIN, GREEN_PIN, GREEN_PIN, BLUE_PIN, BLUE_PIN, GREEN_PIN, RED_PIN, GREEN_PIN};

  playSection(melody, noteDurations, colorTones);
}

void hollyJollySectionD(){
  int melody[] = {NOTE_B4, NOTE_B4, NOTE_B4, NOTE_A4, NOTE_G4, NOTE_F4, NOTE_E4, NOTE_G4, NOTE_G4, NOTE_C5, -1};
  int noteDurations[] = {4, 4, 8, 8, 8, 8, 4, 4, 4, 4,};
  int colorTones[] = {BLUE_PIN, BLUE_PIN, BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, BLUE_PIN, RED_PIN, RED_PIN, BLUE_PIN};

  playSection(melody, noteDurations, colorTones);
}

void hollyJollySectionD1(){
  int melody[] = {NOTE_D5, NOTE_A4, NOTE_B4, NOTE_C5, -1};
  int noteDurations[] = {4, 2, 4, 1};
  int colorTones[] = {GREEN_PIN, BLUE_PIN, GREEN_PIN, RED_PIN};

  playSection(melody, noteDurations, colorTones);
}

void hollyJollySectionD2(){
  int melody[] = {NOTE_D5, NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, NOTE_G5, NOTE_C6, -1};
  int noteDurations[] = {2, 1, 2, 1, 4, 4, 4};
  int colorTones[] = {BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, BLUE_PIN, RED_PIN, GREEN_PIN};

  playSection(melody, noteDurations, colorTones);
}

void nokiaTune(){
  int melody[] = {NOTE_E7, NOTE_D7, NOTE_FS6, NOTE_GS6, NOTE_CS7, NOTE_B6, NOTE_D6, NOTE_E6, NOTE_B6, NOTE_A6, NOTE_CS6, NOTE_E6, NOTE_A6, -1};
  int noteDurations[] = {8, 8, 4, 4, 8, 8, 4, 4, 8, 8, 4, 4, 1};
  int colorTones[] = {BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, RED_PIN};
  
  playSection(melody, noteDurations, colorTones);
}

void playSection(int melody[], int noteDurations[], int colorTones[]){
  for(int curNote = 0; melody[curNote] != -1; curNote++){
    //unsigned int noteDuration = ((1000 * 60) / tempo) / (noteDurations[curNote] / 4);
    int noteDuration = 1000 / noteDurations[curNote];
    tone(SPEAKER_PIN, melody[curNote], noteDuration);
    digitalWrite(colorTones[curNote], HIGH);
    delay(noteDuration);
    digitalWrite(colorTones[curNote], LOW);
    delay(noteDuration * 0.30);
    noTone(SPEAKER_PIN);
  }  
}

void hollyJolly(){
  int tempo = 120;
  
  hollyJollySectionA();
  hollyJollySectionB();
  hollyJollySectionB1();
  hollyJollySectionA();
  hollyJollySectionB();
  hollyJollySectionB2();
  hollyJollySectionC();
  hollyJollySectionA();
  
  hollyJollySectionD();
  hollyJollySectionD1();
  hollyJollySectionC();

  hollyJollySectionA();
  hollyJollySectionD();
  hollyJollySectionD2();
}

void jpSec1(){
//  FrGrFCDrA#rG
  int melody[] = {NOTE_AS4, NOTE_A4, NOTE_AS4, 0, NOTE_AS4, NOTE_A4, NOTE_AS4, 0, NOTE_AS4, NOTE_A4, NOTE_AS4, 0, NOTE_C5, NOTE_C5, 0, NOTE_DS5, NOTE_DS5, -1}; //NOTE_D5, NOTE_AS5, NOTE_D5, NOTE_AS5, NOTE_F5, NOTE_D5, NOTE_AS5, NOTE_D5, -1};
  int noteDurations[] = {4, 4, 1, 2, 4, 4, 1, 2, 4, 4, 2, 4, 4, 2, 4, 4, 1};//, 4, 4, 2, 4, 4, 4, 4, 2};
  int colorTones[] = {BLUE_PIN, GREEN_PIN, BLUE_PIN, 0, BLUE_PIN, GREEN_PIN, BLUE_PIN, 0, BLUE_PIN, GREEN_PIN, BLUE_PIN, 0, BLUE_PIN, BLUE_PIN, 0, RED_PIN, RED_PIN};//, RED_PIN, GREEN_PIN, RED_PIN, GREEN_PIN, BLUE_PIN, GREEN_PIN, RED_PIN, GREEN_PIN};

  playSection(melody, noteDurations, colorTones);
}

void jp(){
  jpSec1();
}

void nyanCat(){
  int melody[] = {NOTE_G5,  NOTE_A5, NOTE_E5, NOTE_E5, 0, NOTE_C5, NOTE_DS5, NOTE_D5, NOTE_C5, 0, NOTE_C5, NOTE_D5, 
  NOTE_DS5, NOTE_DS5, NOTE_D5, NOTE_C5, NOTE_D5, NOTE_E5, NOTE_G5, NOTE_A5, NOTE_E5, NOTE_G5, NOTE_D5, NOTE_E5, NOTE_C5, NOTE_D5, NOTE_C5,
  NOTE_E5, NOTE_G5, NOTE_A5, NOTE_E5, NOTE_G5, NOTE_D5, NOTE_E5, NOTE_C5, NOTE_DS5, NOTE_E5, NOTE_DS5, NOTE_D5, NOTE_C5, NOTE_D5,
  NOTE_DS5, NOTE_C5, NOTE_D5, NOTE_E5, NOTE_G5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_C5, NOTE_D5, NOTE_C5, NOTE_D5,
  -1};
  int noteDurations[] = {4, 4, 8, 8, 8, 8, 8, 8, 8, 8, 4, 4, 
  4,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  4, 4, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  4, 8, 8, 8, 8, 8, 8, 8, 8, 4, 4, 4
  };
  int colorTones[] = {RED_PIN, GREEN_PIN, BLUE_PIN, RED_PIN, 0, BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, BLUE_PIN, 0, RED_PIN, BLUE_PIN, 
  BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, BLUE_PIN, BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, BLUE_PIN, BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, BLUE_PIN, 
  BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, BLUE_PIN, BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, BLUE_PIN, BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN,
  BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, BLUE_PIN, BLUE_PIN, GREEN_PIN, RED_PIN, BLUE_PIN, BLUE_PIN, BLUE_PIN, GREEN_PIN};
  playSection(melody, noteDurations, colorTones);
}

void nyanCatB(){
  int melody[] = {NOTE_FS4, NOTE_GS4, NOTE_DS4, NOTE_FS4, NOTE_GS4, NOTE_DS4, NOTE_B4, NOTE_CS5, NOTE_DS5, NOTE_B4, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_FS5, 
  -1};
  int noteDurations[] = {4, 8, 8, 4, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8
  };
  int colorTones[] = {};

  playSection(melody, noteDurations, colorTones);
}

void loop() {
  int button1State;
  int button2State;
  button1State = digitalRead(BUTTON1_PIN);
  button2State = digitalRead(BUTTON2_PIN);
  if(button1State == LOW){
    hollyJolly();
  }
  if(button2State == LOW){
    nokiaTune();
    /*jp();
    nyanCat();
    nyanCat();*/
  }
}
